# Monthly sum of Heating Degree Days

In this repository is published the monthly sum of Heating Degree Days (HDD) from 1950 to 2016, using as base temperature 15.5°C.
The dataset used as input to generete the data was produced by the [European Climate Assessment & Dataset](http://www.ecad.eu/download/ensembles/download.php).
The dataset is using as CRS the WGS84 (EPSG:4326).
The values contained in the raster must be divided by 100. to get the HDD in°C.

![alt text](data/hdd_1550_sum_1950_01.png "HHD at 15.5°C for January 1950")


## Repository structure

Files:
```
datapackage.json  -- Datapackage JSON file with themain meta-data
data/*.tif        -- GeoTIFF raster files
data/*.color      -- GRASS GIS raster color rules
data/proj.txt     -- Projection information in PROJ.4 format
data/init.txt     -- GRASS GIS space time raster dataset information
data/list.txt     -- Time series file, lists all maps by name with interval
                     time stamps in ISO-Format. Field separator is |
data/metadata.txt -- The output of t.info
```
This space time raster dataset was exported with t.rast.export of GRASS GIS 7


## License

The license is still to be defined, the **European Climate Assessment & Dataset** required to apply the [ECA&D data policy](http://www.ecad.eu/documents/ECAD_datapolicy.pdf), therefore for non-commercial research and non-commercial education projects only.


## Acknowledgement

We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.

We acknowledge the E-OBS that provide the original dataset used to generate the dataset contained in this repository from:
* the EU-FP6 project [ENSEMBLES](http://ensembles-eu.metoffice.com) and 
* the data providers in the [ECA&D](http://www.ecad.eu) project.


